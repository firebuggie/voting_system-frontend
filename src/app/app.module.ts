import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './modules/material.module';
import {MainComponent} from './components/main/main.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {UserModule} from './modules/user/user.module';
import {AdminModule} from './modules/admin/admin.module';
import {LoginComponent} from './components/login/login.component';
import {AdminLoginComponent} from './components/admin-login/admin-login.component';
import {JwtInterceptor} from './interceptors/jwt.interceptor';
import {AuthService} from './services/auth.service';
import {AdminGuard} from './guards/admin.guard';
import { ReportComponent } from './components/report/report.component';

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        LoginComponent,
        AdminLoginComponent,
        ReportComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NoopAnimationsModule,
        MaterialModule,
        HttpClientModule,
        AdminModule,
        UserModule
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        AuthService,
        AdminGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
