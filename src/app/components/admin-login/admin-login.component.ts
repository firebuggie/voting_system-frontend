import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {

  loginForm: FormGroup;
  loginInprogress = false;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private fb: FormBuilder,
      private authService: AuthService,
      private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    this.loginInprogress = true;

    const adminData = {
      login: this.loginForm.get('login').value,
      password: this.loginForm.get('password').value
    };
    let loggedUser;
    this.loginInprogress = true;
    this.authService.loginAdmin(adminData).subscribe((data) => {
      loggedUser = {
        id: 2323423,
        login: 'Admin'
      };
      this.loginInprogress = false;
      localStorage.setItem('token', JSON.stringify(data.token));
      localStorage.setItem('currentUser', JSON.stringify(loggedUser));
      this.router.navigate(['admin']);
    }, () => {
      this.loginInprogress = false;
    });
  }

}
