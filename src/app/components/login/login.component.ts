import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {UserAuthData} from '../../interfaces/user-auth-data';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginInprogress = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      pesel: ['', Validators.required],
      income: ['', Validators.required],
      idNumber: ['', Validators.required],
      rulesCheckbox: ['', Validators.required]
    });
  }

  onAdminLoginClick(): void {
    this.router.navigate(['admin-login']);
  }

  onSubmit(): void {
      const userToLogin: UserAuthData = {
        firstName: this.loginForm.get('firstName').value,
        lastName: this.loginForm.get('lastName').value,
        pesel: this.loginForm.get('pesel').value,
        income: this.loginForm.get('income').value,
        idNumber: this.loginForm.get('idNumber').value
      };
      let loggedUser;
      this.loginInprogress = true;
      this.authService.loginUser(userToLogin).subscribe((data) => {
        loggedUser = {
          id: 45345345,
          login: 'User'
        };
        this.loginInprogress = false;
        localStorage.setItem('token', JSON.stringify(data.token));
        localStorage.setItem('currentUser', JSON.stringify(loggedUser));
        this.router.navigate(['']);
      }, () => {
        this.loginInprogress = false;
      });
  }
}
