import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {NavigationEnd, Router} from '@angular/router';
import {filter, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {

  showReportIcon: boolean;
  logout = this.authService.logout.bind(this.authService);

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnInit(): void {
    this.router.events.pipe(filter(e => e instanceof NavigationEnd),
        takeUntil(this.ngUnsubscribe)).subscribe((event: NavigationEnd) => {
      this.showReportIcon = event.url !== '/report';
    });
  }

  redirectToReport(): void {
    this.router.navigate(['/report']);
  }

}
