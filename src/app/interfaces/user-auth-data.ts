export interface UserAuthData {
  firstName: string;
  lastName: string;
  pesel: number;
  income: string;
  idNumber: number;
}
