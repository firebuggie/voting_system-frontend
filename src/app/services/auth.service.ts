import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Router} from '@angular/router';
import {appConfig} from '../config';
import {UserAuthData} from '../interfaces/user-auth-data';
import {AdminAuthData} from '../interfaces/admin-auth-data';

@Injectable()
export class AuthService {

  constructor(
      private http: HttpClient,
      private router: Router
  ) {}

  loginAdmin(data: AdminAuthData): Observable<any> {
    return of(true);
  }

  loginUser(data: UserAuthData): Observable<any> {
    // return this.http.post<any>(appConfig.login,{ data });
    return of(true);
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login']);
  }
}
