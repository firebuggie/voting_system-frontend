import {AdminPanelComponent} from './components/admin-panel/admin-panel.component';
import {VoteTypeComponent} from './components/vote-type/vote-type.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from './components/admin/admin.component';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            {
                path: '',
                component: AdminPanelComponent
            },
            {
                path: 'vote-type',
                component: VoteTypeComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {
}
