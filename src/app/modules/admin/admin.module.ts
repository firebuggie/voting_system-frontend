import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminPanelComponent} from './components/admin-panel/admin-panel.component';
import {VoteTypeComponent} from './components/vote-type/vote-type.component';
import {AdminComponent} from './components/admin/admin.component';
import {AdminRoutingModule} from './admin-routing.module';
import {VotingService} from './services/voting.service';

@NgModule({
    declarations: [
        AdminPanelComponent,
        VoteTypeComponent,
        AdminComponent
    ],
    imports: [
        CommonModule,
        AdminRoutingModule
    ],
    providers: [
        VotingService
    ]
})
export class AdminModule {
}
