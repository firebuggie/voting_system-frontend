import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {appConfig} from '../../../config';

@Injectable()
export class VotingService {

    constructor(
        private http: HttpClient
    ) {
    }

    getAllVoting(): Observable<any> {
        return this.http.get<any>(appConfig.getVoting);
    }

}
