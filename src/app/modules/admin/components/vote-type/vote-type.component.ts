import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vote-type',
  templateUrl: './vote-type.component.html',
  styleUrls: ['./vote-type.component.scss']
})

export class VoteTypeComponent implements OnInit {
    mockedData = [
    {
      id: '1',
      name: 'Jan',
      lastName: 'Kowalski',
      voteType: 'Samorządowe',
      listNumber: '1',
      listPosition: '10'
    },
    {
      id: '1',
      name: 'Jan',
      lastName: 'Kowalski',
      voteType: 'Samorządowe',
      listNumber: '1',
      listPosition: '10'
    },
    {
      id: '1',
      name: 'Jan',
      lastName: 'Kowalski',
      voteType: 'Samorządowe',
      listNumber: '1',
      listPosition: '10'
    },
    {
      id: '1',
      name: 'Jan',
      lastName: 'Kowalski',
      voteType: 'Samorządowe',
      listNumber: '1',
      listPosition: '10'
    },
    {
      id: '1',
      name: 'Jan',
      lastName: 'Kowalski',
      voteType: 'Samorządowe',
      listNumber: '1',
      listPosition: '10'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
