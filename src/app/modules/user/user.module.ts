import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VotingListComponent} from './components/voting-list/voting-list.component';
import {UserRoutingModule} from './user-routing.module';

@NgModule({
    declarations: [VotingListComponent],
    imports: [
        CommonModule,
        UserRoutingModule
    ]
})
export class UserModule {
}
