import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VotingListComponent} from './components/voting-list/voting-list.component';

const routes: Routes = [
    {
        path: '',
        component: VotingListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule {
}
