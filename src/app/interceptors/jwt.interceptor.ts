import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MatSnackBar} from '@angular/material';
import {tap} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(public snackBar: MatSnackBar, private authService: AuthService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = JSON.parse(localStorage.getItem('token'));
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                }
            });
        }

        return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
        }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    this.authService.logout();
                    this.snackBar.open('Session expired', 'Ok', {
                        duration: 2000
                    });
                } else if (err.error.message) {
                    this.snackBar.open(err.error.message, 'Ok', {
                        duration: 2000
                    });
                } else {
                    this.snackBar.open('Unknown error occured', 'Ok', {
                        duration: 2000
                    });
                }
            }
        }));
    }
}
